.PP
The "ndn" module implements functionality to match ndn packets and to perform load balancing among multiple machines, through an iptables match rule.
.TP
\fB\-\-exact\-match\fP
Match if an interest has "max suffix" equals to "min suffix". This means that the data name must exactly match the interest name
.PP
Example
.PP

