/*
 *	xt_ndn - Xtables module to ndn load balancer
 * 	Lorenzo Bracciale - University of Rome "Tor Vergata"
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License; either
 *	version 2 of the License, or any later version, as published by the
 *	Free Software Foundation.
 */
#include <linux/dccp.h>
#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/string.h>
#include <linux/ctype.h>
#include <linux/icmp.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/sctp.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <net/ip.h>
#include <net/ipv6.h>
#include <linux/netfilter/x_tables.h>
#include <linux/netfilter_ipv6/ip6_tables.h>
#include "xt_ndn.h"
#include "compat_xtables.h"
#if defined(CONFIG_IP6_NF_IPTABLES) || defined(CONFIG_IP6_NF_IPTABLES_MODULE)
#	define WITH_IPV6 1
#endif
#ifndef NEXTHDR_IPV4
#	define NEXTHDR_IPV4 4
#endif

MODULE_AUTHOR("Lorenzo Bracciale");
MODULE_DESCRIPTION("Xtables: match for ndn load balancer");
MODULE_LICENSE("GPL");
MODULE_ALIAS("ndnloadbalancer");

struct ndn_tlv_t {
    __u8 type;
    uint32_t len;
    __u8 *value;
}; 

typedef struct ndn_tlv_t ndn_tlv;

/*
 * Jenkins one at a time hash function
 */
static
uint32_t hash_function(char *key, size_t len)
{
    uint32_t hash, i;
    for(hash = i = 0; i < len; ++i)
    {
        hash += key[i];
        hash += (hash << 10);
        hash ^= (hash >> 6);
    }
    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);
    return hash;
}
/* Parse type-len-value structure */
static void parse_ndn_tlv(__u8 *data, ndn_tlv* tlv) 
{
    // set type
    tlv->type = *data;
    // set len and value
    if (*(data+1) < 253) {
        tlv->len = (__u8)*(data +1); // len 1 byte
        tlv->value = data + 2;
    }
    else if (*(data+1) == 253) {
        tlv->len = ntohs(*(data + 2)); // len 2 byte
        tlv->value = data + 4;
    }
    else if (*(data+1) == 254) {
        tlv->len = ntohl(*(data + 2)); // len 4 byte
        tlv->value = data + 6;
    }
    else if (*(data+1) == 255) {
        //todo 
        tlv->len = ntohl(*(data + 2)); //len 8 byte
        tlv->value = data + 10;
    }
}

/* Parse  minsuffix */
static
int32_t parse_ndn_minmaxsuffix(__u8 *data, uint32_t namelen) 
{
    ndn_tlv tlv;
	parse_ndn_tlv(data, &tlv); 
	return (__u8)*tlv.value;
}
/* parse name compoenents, concatenate strings and write on name ptr */

void 
parse_ndn_namecomponents(__u8 *data, uint32_t data_len, __u8 *name, uint32_t *name_len) 
{
    // called inside name type
    // print namedcomponent values
    char *buf;
    __u8* p = data;
    ndn_tlv tlv;

    if (name == NULL) return;

    while (p < data + data_len)
    {
        parse_ndn_tlv(p, &tlv); 

        if (tlv.type == XT_NDN_TYPE_NAMECOMPONENT) {
            printk(KERN_ALERT "Trovato name component lungo %u: ", tlv.len);
            // print: 3 is to account the priority prefix
            if (isascii(tlv.value[0]))
                buf = kmalloc(tlv.len + 3, GFP_KERNEL);
            else
                buf = kmalloc(2 * tlv.len + 3, GFP_KERNEL);

            if (buf == NULL) return;
            memcpy(buf, "   ", 3);

            if (isascii(tlv.value[0]))
                memcpy(buf + 3, tlv.value, tlv.len);
            else
                bin2hex(buf + 3, tlv.value, tlv.len);

            printk(buf);
            //printk("\n");

            kfree(buf);
			// prepend slash
			name[*name_len] = '/';
            printk(KERN_ALERT "NAME:");
            printk(name);
			(*name_len)++;
			
            // append to name
            memcpy(name + *name_len, tlv.value, tlv.len);
            *name_len += tlv.len;
        } else {
            printk(KERN_ALERT "Found other field in name with code %X\n", *p);
        }

        p = tlv.value + tlv.len;
    
    }

}


static bool
ndn_mt(const struct sk_buff *skb, struct xt_action_param *par)
{
	const struct xt_ndn_mtinfo2 *info = par->matchinfo;
	const struct iphdr *iph = ip_hdr(skb);
	struct udphdr *udph;     // udp header struct 
	uint16_t sport, dport;   // source & dest port
    int32_t min_suffix = -1, max_suffix = -1;

    char *name = NULL;
    uint32_t name_len = 0;
    uint32_t hash_value = 0;

	size_t lenpre, lenname;

    ndn_tlv tlv, tlv2;
	__u8 *payload, *curr = NULL, *end = NULL, *curr2 = NULL;
	__u8 ndn_packet_type;
	//info->hash_match;
	//info->hash_max;

		
	if (iph->protocol != IPPROTO_UDP){
		// not an udp packet
		return false;
	}

	/* We are not interested in fragments */
	if (iph->frag_off & htons(IP_OFFSET))
		return false;

	/* No replies to physical multicast/broadcast */
	/* skb != PACKET_OTHERHOST handled by ip_rcv() */
	if (skb->pkt_type != PACKET_HOST)
		return false;

	/*
	 * Our naive response construction does not deal with IP
	 * options, and probably should not try.
	 */
	if (ip_hdrlen(skb) != sizeof(struct iphdr))
		return NF_DROP;

	udph = (struct udphdr *)(skb_network_header(skb) + ip_hdrlen(skb));
	// http://www.roman10.net/2011/07/23/how-to-filter-network-packets-using-netfilterpart-2-implement-the-hook-function/
	// udph = (struct udphdr *)skb_transport_header(skb); 

	sport = ntohs((unsigned short int) udph->source);
	dport = ntohs((unsigned short int) udph->dest);

	if (sport != 6363 && dport != 6363) {
		return false;
	} else {
		//printk(KERN_ALERT "ndn NDN packet received on 6363\n");
	}
		
	// without pagination
	// skb_headlen(const struct sk_buff *skb) 
    printk(KERN_ALERT "--- check if NDN match ---- \n");

	payload = skb->data + ip_hdrlen(skb) + 8; //skb_header_pointer(skb, ip_hdrlen(skb) + 16, int len, void *buffer)

	//payload = (__u8 *)udph + 8;
    
	// ndn is TLV

    parse_ndn_tlv(payload, &tlv);
	ndn_packet_type = tlv.type;


	end = tlv.value + tlv.len;
	curr = tlv.value;

	// parse all the fields	
	while(curr < end) {
		parse_ndn_tlv(curr, &tlv);

		if (tlv.type == XT_NDN_TYPE_NAME) {
			// name type found
			name = kmalloc(XT_NDN_MAX_NAME_LEN, GFP_KERNEL);
			if (!name) return false;

            // init name with 0s
            memset(name, 0, XT_NDN_MAX_NAME_LEN);

			parse_ndn_namecomponents(tlv.value, tlv.len, name, &name_len);
			// name contains a string with the name

			// compute the hash
			hash_value = hash_function(name, name_len);
			//printk(KERN_ALERT "Hash value = %u\n", hash_value);


		} else if (ndn_packet_type == XT_NDN_TYPE_INTEREST && tlv.type == XT_NDN_TYPE_SELECTORS) {
            // scan inside selectors to spot min and max suffix fields
            curr2 = tlv.value;
            while (curr2 < tlv.value + tlv.len) {
                parse_ndn_tlv(curr2, &tlv2);
                //printk(KERN_ALERT "Primo byte type: %X\n", curr2[0]);
                if (tlv2.type == XT_NDN_TYPE_MINSUFFIX) {
                    min_suffix = parse_ndn_minmaxsuffix(tlv2.value, tlv2.len);
                    //printk(KERN_ALERT "Min suffix found: %u\n", min_suffix);
                } else if (tlv2.type == XT_NDN_TYPE_MAXSUFFIX) {
                    max_suffix = parse_ndn_minmaxsuffix(tlv2.value, tlv2.len);
                    //printk(KERN_ALERT "Max suffix found: %u\n", max_suffix);
                }
                curr2 = tlv2.value + tlv2.len;
             }
		}
		curr = tlv.value + tlv.len;
	} 

	if (min_suffix != max_suffix && min_suffix > 0 && min_suffix > 0) {
        printk(KERN_ALERT "Min suffix != Max suffix\n");
        if (info->flags & XT_NDN_EXACT_MATCH) {
            printk(KERN_ALERT "Not exact match!: \n");
            if (name) kfree(name);
            return false;
        }
	}


    if (hash_value != 0 && info->flags & XT_NDN_HASH) {
        printk(KERN_ALERT "hash value = %u ; hash modulo = %u ; hash match -1 = %u!: \n", hash_value, hash_value % info->hash_max, info->hash_match - 1);
        if (hash_value % info->hash_max == info->hash_match - 1) {
            printk(KERN_ALERT "Hash match\n");
        } else {
            printk(KERN_ALERT "Hash NOT match\n");
            if (name) kfree(name);
            return false;
        }
    }

    if (name && info->flags & XT_NDN_PREFIX) {
		// start with prefix?
		lenpre = strlen(info->prefix);
	    lenname = strlen(name);
        printk(name);
        printk(info->prefix);
        printk(KERN_ALERT "len name = %u len prefix = %u: \n", lenname, lenpre);
        printk(KERN_ALERT "name starts with len prefix: %u \n", strncmp(name, info->prefix, lenpre));
         printk(KERN_ALERT "name[lenpre] == '/' : %u \n", name[lenpre] == '/');
         printk(KERN_ALERT "name[lenpre] == NULL : %u \n", name[lenpre] == '\0');
        if (lenname < lenpre) {
            if (name) kfree(name);
            return false;
        } else {
            if (!(strncmp(name, info->prefix, lenpre) == 0 && (name[lenpre] == '/' || name[lenpre] == '\0'))) {
                printk(KERN_ALERT "Prefix not MATCH!: \n");
                if (name) kfree(name);
                return false;
            }
        
        }
    }

	if (ndn_packet_type == XT_NDN_TYPE_INTEREST) {
		// interest
        if (info->flags & XT_NDN_DATA) { // if --only-data, return false
             printk(KERN_ALERT "Only data not match\n");
            if (name) kfree(name);
            return false;
        }
	} else if (ndn_packet_type == XT_NDN_TYPE_DATA) {
		// data
        if (info->flags & XT_NDN_INTEREST) { // if --only-interest, return false
             printk(KERN_ALERT "Only interest not match\n");
            if (name) kfree(name);
            return false;
        }
	} 


    if (name) kfree(name);
     printk(KERN_ALERT "NDN match \n");
	return true;

}


static struct xt_match ndn_mt_reg[] __read_mostly = {
	{
		.name           = "ndn",
		.revision       = 2,
		.family         = NFPROTO_IPV4,
		.match          = ndn_mt,
		.matchsize      = sizeof(struct xt_ndn_mtinfo2),
		.me             = THIS_MODULE,
	}
};

static int __init ndn_mt_init(void)
{
	printk(KERN_ALERT "NDN module loaded");
	return xt_register_matches(ndn_mt_reg, ARRAY_SIZE(ndn_mt_reg));
}

static void __exit ndn_mt_exit(void)
{
	xt_unregister_matches(ndn_mt_reg, ARRAY_SIZE(ndn_mt_reg));
}

module_init(ndn_mt_init);
module_exit(ndn_mt_exit);
