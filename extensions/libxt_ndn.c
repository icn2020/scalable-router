#include <getopt.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xtables.h>
#include "xt_ndn.h"
#include "compat_user.h"

enum {
	F_EXACT = 1 << 0,
	F_HASH = 1 << 1,
    F_INTEREST = 1 << 2,
    F_DATA = 1 << 3,
    F_PREFIX = 1 << 4,
};
//XT_NDN_MASK = XT_NDN_EXACT_MATCH | XT_NDN_INTEREST | XT_NDN_DATA

static void ndn_mt_help(void)
{
	printf(
"ndn options:\n"
"    --exact-match      Match against NDN packets with same min and max suffix components\n"
"    --hash n:nmax      Match the n-th hash over nmax hashes. n must be major equals to 1, and minor-equal to nmax.\n"
"    --only-interest    Match only interest packet \n"
"    --only-data        Match only data packet\n"
"    --prefix           Match packets whose name start with a given prefix\n"
);
}

static const struct option ndn_mt_opts[] = {
	{.name = "exact-match", .has_arg = false, .val = 'e'},
	{.name = "hash", .has_arg = true, .val = 'h'},
	{.name = "only-interest", .has_arg = false, .val = 'i'},
	{.name = "only-data", .has_arg = false, .val = 'd'},
	{.name = "prefix", .has_arg = true, .val = 'p'},
	{NULL},
};

static void ndn_mt_init(struct xt_entry_match *match)
{
	struct xt_ndn_mtinfo2 *info = (void *)match->data;
	// set as default
	info->flags = 0; //XT_NDN_EXACT_MATCH;
}

static int ndn_mt_parse(int c, char **argv, int invert, unsigned int *flags,
                           const void *entry, struct xt_entry_match **match)
{
	struct xt_ndn_mtinfo2 *info = (void *)(*match)->data;
	unsigned int hash, hash_max;
	char *end;

	switch (c) {
        case 'e': /* --exact-match */
            xtables_param_act(XTF_ONLY_ONCE, "ndn", "--exact-match*", *flags & F_EXACT);
            //info->flags &= ~XT_NDN_MASK;
            info->flags |= XT_NDN_EXACT_MATCH;
            *flags |= F_EXACT;
            return true;
        case 'h': /* --hash */
            xtables_param_act(XTF_ONLY_ONCE, "ndn", "--hash", *flags & F_HASH);
            if (!xtables_strtoui(optarg, &end, &hash, 0, ~0U))
                xtables_param_act(XTF_BAD_VALUE, "ndn", "--hash", optarg);
            if (*end == ':') {
                if (!xtables_strtoui(end + 1, &end, &hash_max, 0, ~0U))
                    xtables_param_act(XTF_BAD_VALUE, "ndn",
                              "--hash", optarg);
            } else {
                xtables_param_act(XTF_BAD_VALUE, "ndn", "--hash", optarg);
            }
            if (*end != '\0')
                //xtables_param_act(XTF_BAD_VALUE, "ndn", "--hash", optarg);
                xtables_param_act(XTF_BAD_VALUE, "ndn", "--hash", optarg);
            if (hash > hash_max || hash_max == 0 || hash == 0)
                xtables_param_act(XTF_BAD_VALUE, "ndn", "--hash", optarg);
            info->hash_match = hash;
            info->hash_max = hash_max;
            info->flags |= XT_NDN_HASH;
            *flags |= F_HASH;
            return true;
        case 'i':
            xtables_param_act(XTF_ONLY_ONCE, "ndn", "--only-interest*", *flags & F_INTEREST);
            //info->flags &= ~XT_NDN_MASK;
            info->flags |= XT_NDN_INTEREST;
            *flags |= F_INTEREST;
            return true;
        case 'd':
            xtables_param_act(XTF_ONLY_ONCE, "ndn", "--only-data*", *flags & F_DATA);
            //info->flags &= ~XT_NDN_MASK;
            info->flags |= XT_NDN_DATA;
            *flags |= F_DATA;
            return true;
        case 'p':
            xtables_param_act(XTF_ONLY_ONCE, "ndn", "--prefix", *flags & F_PREFIX);
            if (*optarg == '\0')
                xtables_param_act(XTF_BAD_VALUE, "ndn", "--prefix", optarg);
            if (optarg[0] != '/')
                xtables_param_act(XTF_BAD_VALUE, "ndn", "--prefix should start with /", optarg);
            if (strstr(optarg, "//") != NULL) 
                xtables_param_act(XTF_BAD_VALUE, "ndn", "--prefix should not have //", optarg);
            strncpy(info->prefix, optarg, XT_NDN_MAX_NAME_LEN - 1);
            info->flags |= XT_NDN_PREFIX;
            *flags |= F_PREFIX;
            return true;

	}
	return false;
}

static void ndn_mt_check(unsigned int flags)
{
    if (flags == 0) 
		xtables_error(PARAMETER_PROBLEM, "ndn: You must specify at least one option ");
    if (flags & F_DATA && flags & F_INTEREST)
		xtables_error(PARAMETER_PROBLEM, "ndn: You cannot specify only-data and only-interest");
}

static void ndn_mt_save(const void *ip, const struct xt_entry_match *match)
{
	const struct xt_ndn_mtinfo2 *info = (const void *)match->data;

	if (info->flags & XT_NDN_EXACT_MATCH)
		printf(" --exact-match");
	if (info->flags & XT_NDN_INTEREST)
		printf(" --only-interest");
	if (info->flags & XT_NDN_DATA)
		printf(" --only-data");
    if (info->flags & XT_NDN_HASH) {
        printf(" --hash");
        printf("%u:%u ", (unsigned int)info->hash_match,
               (unsigned int)info->hash_max);
    }
    if (info->flags & XT_NDN_PREFIX)
        printf(" --prefix %s", info->prefix);
}

static void ndn_mt_print(const void *ip, const struct xt_entry_match *match,
                            int numeric)
{
	printf(" -m ndn");
	ndn_mt_save(ip, match);
}

static struct xtables_match ndn_mt_reg = {
	.version        = XTABLES_VERSION,
	.name           = "ndn",
	.revision       = 2,
	.family         = NFPROTO_UNSPEC,
	.size           = XT_ALIGN(sizeof(struct xt_ndn_mtinfo2)),
	.userspacesize  = XT_ALIGN(sizeof(struct xt_ndn_mtinfo2)),
	.init           = ndn_mt_init,
	.help           = ndn_mt_help,
	.parse          = ndn_mt_parse,
	.final_check    = ndn_mt_check,
	.print          = ndn_mt_print,
	.save           = ndn_mt_save,
	.extra_opts     = ndn_mt_opts,
};

static void _init(void)
{
	xtables_register_match(&ndn_mt_reg);
}
