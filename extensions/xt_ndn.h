#ifndef _LINUX_NETFILTER_XT_NDN_H
#define _LINUX_NETFILTER_XT_NDN_H

#define XT_NDN_MAX_NAME_LEN 4096
// ndn type list: https://named-data.net/doc/ndn-tlv/types.html#types
#define XT_NDN_TYPE_INTEREST 0x05
#define XT_NDN_TYPE_DATA 0x06
#define XT_NDN_TYPE_NAME 0x07
#define XT_NDN_TYPE_NAMECOMPONENT 0x08
#define XT_NDN_TYPE_SELECTORS 0x09
#define XT_NDN_TYPE_MINSUFFIX 0x0D
#define XT_NDN_TYPE_MAXSUFFIX 0x0E

enum {
	XT_NDN_EXACT_MATCH = 1 << 0,
	XT_NDN_INTEREST = 1 << 1,
	XT_NDN_DATA = 1 << 2,
    XT_NDN_HASH = 1 << 3,
    XT_NDN_PREFIX = 1 << 4,
};

struct xt_ndn_mtinfo2 {
	u_int32_t hash_match, hash_max;
	u_int32_t flags;
    char prefix[XT_NDN_MAX_NAME_LEN];
};



#endif /* _LINUX_NETFILTER_XT_NDN_H */
